FROM node:10

LABEL description="NodeJs setup dockerfile"

# # Create app directory
RUN mkdir -p /usr/app
WORKDIR /usr/app

# Installing dependencies
COPY ./package*.json /usr/app/
RUN npm install

# Copying source files
COPY ./ /usr/app

RUN npm run dev-server

# Building app
RUN npm run build
EXPOSE 3000

# Running the app
CMD "npm" "run" "dev"
